package com.project.GymManagementSystem.Controller;


import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Model.Product;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Service.PackageService;
import com.project.GymManagementSystem.Service.ProductService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.PRODUCT)
public class ProductController extends BaseController{

    @Autowired
    ProductService productService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.PRODUCTLIST).addObject("Products",productService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.PRODUCT);
        modelAndView.addObject("Product",new Product());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Product") @Valid Product product, BindingResult bindingResult){
        productService.save(product);
        return redirectTo(UrlRoutes.PRODUCT);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer productId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Product Id = "+productId);
        Optional<Product> product = productService.findById(productId);
        ModelAndView modelAndView = new ModelAndView(View.PRODUCT);
        modelAndView.addObject("Product",product);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer productId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Product Id = "+productId);
        productService.deleteById(productId);
        return redirectTo(UrlRoutes.PRODUCT);
    }

}

