package com.project.GymManagementSystem.Repository;

import com.project.GymManagementSystem.Model.Brand;
import com.project.GymManagementSystem.Model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BrandRepository extends JpaRepository<Brand,Integer> {

    public List<Brand> findAll();

    public Optional<Brand> findById(Integer brandId);


}
