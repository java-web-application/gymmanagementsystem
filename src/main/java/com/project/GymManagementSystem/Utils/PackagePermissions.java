package com.project.GymManagementSystem.Utils;

public class PackagePermissions {

    public static final String ADDITIONAL_COSTS = "ADDITIONAL-COSTS";
    public static final String INVESTORS = "INVESTORS";
    public static final String REPORTS = "REPORTS";
    public static final String RENTS = "RENTS";
}
