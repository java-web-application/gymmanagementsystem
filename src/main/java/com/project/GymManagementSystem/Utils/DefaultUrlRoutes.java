package com.project.GymManagementSystem.Utils;
public class DefaultUrlRoutes {
    //
    public static final String INDEX = "/";
    public static final String CREATE = "/create";
    public static final String CREATE_FORM = "create";
    public static final String EDIT = "/edit";
    public static final String SHOW = "/show";
    public static final String DELETE = "/delete";
    public static final String SAVE = "/save";
    public static final String SAVE_FORM = "save";
    public static final String UPLOAD = "/upload";
    public static final String REGISTER = "/registration";
    public static final String APPROVE = "/approve";
    public static final String UN_APPROVE = "/un-approve";
    public static final String UPDATE = "/update";
    public static final String GOOGLE_LOGIN = "/google-login";
    public static final String LOGIN = "/login";
    public static final String ACCESS_DENIED = "/access-denied";
    public static final String SHOW_ALL = "/show-all/";
}
