package com.project.GymManagementSystem.Utils;

public class PaymentTypeIds {

    public static final int Cash = 1;
    public static final int Payment_Order = 2;
    public static final int Check =3;
}
