package com.project.GymManagementSystem.Utils;

public class View {

    public static final String ACCOUNTHEADLIST = UrlRoutes.ACCOUNTHEAD + "/AccountHeadList";

    public static final String ACCOUNTHEAD = UrlRoutes.ACCOUNTHEAD + "/AccountHead";

    public static final String ACCOUNTSUBGROUPLIST = UrlRoutes.ACCOUNTSUBGROUP + "/AccountSubGroupList";

    public static final String ACCOUNTSUBGROUP = UrlRoutes.ACCOUNTSUBGROUP + "/AccountSubGroup";


    public static final String ACCOUNTGROUPLIST = UrlRoutes.ACCOUNTGROUP + "/AccountGroupList";

    public static final String ACCOUNTGROUP = UrlRoutes.ACCOUNTGROUP + "/AccountGroup";

    public static final String ACCOUNTTYPELIST = UrlRoutes.ACCOUNTTYPE + "/AccountTypeList";

    public static final String ACCOUNTTYPE = UrlRoutes.ACCOUNTTYPE + "/AccountType";


    public static final String COMPANYLIST = UrlRoutes.ORGANIZATION+"/CompanyList";

    public static final String COMPANY = UrlRoutes.ORGANIZATION+"/Company";

    public static final String SUPPLIERLIST = UrlRoutes.SUPPLIER+"/SupplierList";
    public static final String SUPPLIER = UrlRoutes.SUPPLIER+"/Supplier";


    public static final String CUSTOMERLIST = UrlRoutes.CUSTOMER+"/CustomerList";
    public static final String CUSTOMER = UrlRoutes.CUSTOMER+"/Customer";

    public static final String BRANCHLIST = UrlRoutes.ORGANIZATION+"/BranchList";
    public static final String BRANCH = UrlRoutes.ORGANIZATION+"/Branch";


    public static final String DEPARTMENT = UrlRoutes.ORGANIZATION+"/Department";
    public static final String DEPARTMENTLIST = UrlRoutes.ORGANIZATION+"/DepartmentList";

    public static final String DESIGNATIONLIST = UrlRoutes.ORGANIZATION+"/DesignationList";
    public static final String DESIGNATION = UrlRoutes.ORGANIZATION+"/Designation";

    public static final String EMPLOYEE = UrlRoutes.HRM+"/Employee";
    public static final String EMPLOYEELIST = UrlRoutes.HRM+"/EmployeeList";

    public static final String MEMBER = UrlRoutes.MEMBER+"/Member";
    public static final String MEMBERLIST = UrlRoutes.MEMBER+"/MemberList";

    public static final String COACH = UrlRoutes.COACH+"/Coach";
    public static final String COACHLIST = UrlRoutes.COACH+"/CoachList";

    public static final String PACKAGE = UrlRoutes.PACKAGE+"/Package";
    public static final String PACKAGELIST = UrlRoutes.PACKAGE+"/PackageList";

    public static final String BRAND = UrlRoutes.INVENTORY+"/Brand";
    public static final String BRANDLIST = UrlRoutes.INVENTORY+"/BrandList";

    public static final String CATEGORY = UrlRoutes.INVENTORY+"/Category";
    public static final String CATEGORYLIST = UrlRoutes.INVENTORY+"/CategoryList";

    public static final String PRODUCT = UrlRoutes.INVENTORY+"/Product";
    public static final String PRODUCTLIST = UrlRoutes.INVENTORY+"/ProductList";

}
