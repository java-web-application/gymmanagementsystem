package com.project.GymManagementSystem;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GymManagementSystemApplication implements ApplicationRunner {


    public static void main(String[] args) {
        SpringApplication.run(GymManagementSystemApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Hello World From Application Runner Interface");
    }

}
