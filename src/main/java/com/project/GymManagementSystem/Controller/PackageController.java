package com.project.GymManagementSystem.Controller;

import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Service.PackageService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.PACKAGE)
public class PackageController extends BaseController{

    @Autowired
    PackageService memberService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.PACKAGELIST).addObject("Packages",memberService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.PACKAGE);
        modelAndView.addObject("Package",new Package());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Package") @Valid Package member, BindingResult bindingResult){
        memberService.save(member);
        return redirectTo(UrlRoutes.PACKAGE);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer memberId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Package Id = "+memberId);
        Optional<Package> member = memberService.findById(memberId);
        ModelAndView modelAndView = new ModelAndView(View.PACKAGE);
        modelAndView.addObject("Package",member);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer packageId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Package Id = "+packageId);
        memberService.deleteById(packageId);
        return redirectTo(UrlRoutes.PACKAGE);
    }

}
