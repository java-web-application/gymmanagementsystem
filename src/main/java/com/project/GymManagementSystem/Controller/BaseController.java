package com.project.GymManagementSystem.Controller;

import org.springframework.web.servlet.ModelAndView;

public class BaseController {
    public ModelAndView redirectTo(String route) {
        return new ModelAndView("redirect:" + route + "/");
    }
}
