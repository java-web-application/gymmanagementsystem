package com.project.GymManagementSystem.Model;

import javax.persistence.*;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @OneToOne
    private Brand brand;

    @OneToOne
    private Category category;

    private Double buyingPrice;

    private Double sellingPrice;

    private String description;

}
