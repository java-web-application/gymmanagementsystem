package com.project.GymManagementSystem.Model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String code;

    private String name;

    private String quantity;

    private String imageUrl;

    @OneToOne
    private Supplier supplier;
}
