package com.project.GymManagementSystem.Model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Code Cannot Be Null")
    private String code;

    @NotBlank(message = "Name is Required")
    private String name;

    private String creditPeriod;

    private Double creditLimit;

    private String mailingName;

    private String phone;

    private String billByBill;

    @NotBlank(message = "Please provide branch name")
    private String branchName;

    @NotBlank(message = "Please Provide Mobile No")
    private String mobile;

    @NotBlank(message = "Please provide email address")
    @Email(message = "Email is not in a good shape")
    private String email;

    private String bankAccountNumber;

    private String TIN;

    private String narration;

    private String PAN;

    private String CSTNumber;

    private Double openingBalance;

    private String routeId;

    private String areaId;

    @NotBlank(message = "Please Specify your address")
    private String address;

}
