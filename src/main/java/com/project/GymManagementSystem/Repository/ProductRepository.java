package com.project.GymManagementSystem.Repository;

import com.project.GymManagementSystem.Model.Brand;
import com.project.GymManagementSystem.Model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {

    public List<Product> findAll();

    public Optional<Product> findById(Integer productId);
}
