package com.project.GymManagementSystem.Service;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Model.Product;
import com.project.GymManagementSystem.Repository.PackageRepository;
import com.project.GymManagementSystem.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> findAll(){
        return productRepository.findAll();
    }

    public Optional<Product> findById(Integer productId){
        return productRepository.findById(productId);
    }

    public void save(Product product){
        productRepository.save(product);
    }

    public void deleteById(Integer productId){
        productRepository.deleteById(productId);
    }


}
