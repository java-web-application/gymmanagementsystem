package com.project.GymManagementSystem.Repository;

import com.project.GymManagementSystem.Model.Package;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import java.util.Optional;

@Repository
public interface PackageRepository extends JpaRepository<Package,Integer> {

     List<Package> findAll();

     Optional<Package> findById(Integer packageId);

}