package com.project.GymManagementSystem.Controller;

import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.MEMBER)
public class MemberController extends BaseController{

    @Autowired
    MemberService memberService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.MEMBERLIST).addObject("Members",memberService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.MEMBER);
        modelAndView.addObject("Member",new Member());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Member") @Valid Member member, BindingResult bindingResult){
        memberService.save(member);
        return redirectTo(UrlRoutes.MEMBER);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer memberId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Member Id = "+memberId);
        Optional<Member> member = memberService.findById(memberId);
        ModelAndView modelAndView = new ModelAndView(View.MEMBER);
        modelAndView.addObject("Member",member);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer memberId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Member Id = "+memberId);
        memberService.deleteById(memberId);
        return redirectTo(UrlRoutes.MEMBER);
    }

}
