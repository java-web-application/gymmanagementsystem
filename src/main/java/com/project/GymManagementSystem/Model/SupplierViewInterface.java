package com.project.GymManagementSystem.Model;


public interface SupplierViewInterface {

    public Integer getId();

    public String getCode();

    public String getName();

    public String getEmail();

    public String getMobile();

    public String getAddress();
}
