package com.project.GymManagementSystem.Service;

import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MemberService{

    @Autowired
    MemberRepository memberRepository;

    public List<Member> findAll(){
        return memberRepository.findAll();
    }

    public Optional<Member> findById(Integer memberId){
        return memberRepository.findById(memberId);
    }

    public Member findByCode(String memberCode){
        return memberRepository.findByCode(memberCode);
    }

    public void save(Member member){
        memberRepository.save(member);
    }

    public void deleteById(Integer memberId){
        memberRepository.deleteById(memberId);
    }


}
