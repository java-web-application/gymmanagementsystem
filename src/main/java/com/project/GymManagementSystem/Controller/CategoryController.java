package com.project.GymManagementSystem.Controller;

import com.project.GymManagementSystem.Model.Category;
import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Service.CategoryService;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Service.PackageService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.CATEGORY)
public class CategoryController extends BaseController{

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.CATEGORYLIST).addObject("Categories",categoryService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.CATEGORY);
        modelAndView.addObject("Category",new Category());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Category") @Valid Category category, BindingResult bindingResult){
        categoryService.save(category);
        return redirectTo(UrlRoutes.CATEGORY);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer categoryId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Category Id = "+categoryId);
        Optional<Category> category = categoryService.findById(categoryId);
        ModelAndView modelAndView = new ModelAndView(View.CATEGORY);
        modelAndView.addObject("Category",category);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer categoryId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Category Id = "+categoryId);
        categoryService.deleteById(categoryId);
        return redirectTo(UrlRoutes.CATEGORY);
    }

}
