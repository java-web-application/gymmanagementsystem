package com.project.GymManagementSystem.Controller;


import com.project.GymManagementSystem.Model.Supplier;
import com.project.GymManagementSystem.Service.SupplierService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
@RequestMapping(value= UrlRoutes.SUPPLIER)
public class SupplierController extends BaseController {

    @Autowired
    SupplierService supplierService;

    @GetMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView(View.SUPPLIERLIST);
        return new ModelAndView(View.SUPPLIERLIST).addObject("Suppliers",supplierService.findAllAsSupplierViewInterface());
    }
    @GetMapping(value=DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        return new ModelAndView(View.SUPPLIER)
                .addObject("Supplier",new Supplier());
    }
    @PostMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@Valid @ModelAttribute("Supplier") Supplier supplier, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            System.out.println(bindingResult.toString());
            ModelAndView modelAndView = new ModelAndView(View.SUPPLIER);
            return modelAndView;
        }
        supplierService.saveOrUpdate(supplier);
        return redirectTo(UrlRoutes.SUPPLIER);
    }
    @GetMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest request){
        Integer supplierId = Integer.valueOf(request.getParameter("id"));
        Supplier supplier = supplierService.findById(supplierId);
        ModelAndView modelAndView = new ModelAndView(View.SUPPLIER);
        modelAndView.addObject("Supplier",supplier);
        return modelAndView;
    }
    @GetMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest request){
        Integer supplierId = Integer.valueOf(request.getParameter("id"));
        supplierService.deleteById(supplierId);
        return redirectTo(UrlRoutes.SUPPLIER);
    }

}
