package com.project.GymManagementSystem.Service;

import com.project.GymManagementSystem.Model.Category;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Repository.CategoryRepository;
import com.project.GymManagementSystem.Repository.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    public Optional<Category> findById(Integer categoryId){
        return categoryRepository.findById(categoryId);
    }

    public void save(Category category){
        categoryRepository.save(category);
    }

    public void deleteById(Integer categoryId){
        categoryRepository.deleteById(categoryId);
    }


}
