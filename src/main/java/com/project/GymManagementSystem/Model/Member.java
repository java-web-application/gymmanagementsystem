package com.project.GymManagementSystem.Model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String code;

    private String fullName;

    private String email;

    private Date dateOfBirth;

    @OneToOne
    private Coach coach;

    private String phone;

    private String mobile;

    private String gender;

    private String address;

    private String exercisePlan;

    private Double weight;

    private Double height;

    private Date joiningDate;

}
