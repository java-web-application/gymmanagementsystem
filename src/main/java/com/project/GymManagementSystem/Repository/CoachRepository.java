package com.project.GymManagementSystem.Repository;


import com.project.GymManagementSystem.Model.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CoachRepository extends JpaRepository<Coach,Integer> {

    public List<Coach> findAll();

    public Optional<Coach> findById(Integer coachId);

    public Coach findByCode(String coachCode);
}
