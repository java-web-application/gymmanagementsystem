package com.project.GymManagementSystem.Utils;
public class UrlRoutes {

    public static final String PURCHASE = "/purchase";
    public static final String PURCHASE_ORDERS = PURCHASE + "/orders";
    public static final String SALE = "/sale";
    public static final String SALE_ORDERS = SALE + "/orders";
    public static final String SUPPLIER = "/Supplier";
    //public static final String PRODUCT = "/products";


    public static final String PRODUCT_RECIPE = "/product-recipe";


    public static final String REGISTRATION_BASE_URL = "/registration";
    //public static final String ADMIN_EVENT_CATEGORY_BASE_URL = UrlRoutes.ADMIN_BASE_URL + "/event-categories";
    // public static final String ADMIN_EVENT_SUB_CATEGORY_BASE_URL = UrlRoutes.ADMIN_BASE_URL + "/event-sub-categories";
    public static final String ADMIN_BASE_URL = "/admin";
    public static final String ADMIN_DASHBOARD_BASE_URL = UrlRoutes.ADMIN_BASE_URL + "/dashboard";
    public static final String USER_PROFILE_BASE_URL = "/user-profile";
    public static final String ADMIN_USER_PROFILE_BASE_URL = UrlRoutes.ADMIN_BASE_URL + "/user-profiles";
    public static final String ROLE_BASE_URL = "/admin/roles";
    public static final String PRIVILEGE_BASE_URL = "/admin/privileges";
    public static final String AGENT_BASE_URL = "/admin/agents";
    public static final String BUILDING_TYPE_BASE_URL = "/admin/building-types";
    public static final String COUNTRY_BASE_URL = "/admin/countries";
    public static final String CONSTRUCTION_STATUS_BASE_URL = "/admin/construction-statuses";
    public static final String CITY_BASE_URL = "/admin/cities";
    public static final String CITY_AREA_BASE_URL = "/admin/city-areas";
    public static final String CLIENT_BASE_URL = "/admin/clients";
    public static final String INVESTOR_BASE_URL = "/admin/investors";
    public static final String INVESTOR_LIST_URL = "/get-investor-list";
    public static final String LOCATION_BASE_URL = "/admin/locations";
    public static final String MEASURING_UNIT_BASE_URL = "/admin/measuring-units";
    public static final String MISCELLANEOUS_BASE_URL = "/properties/sale-records/miscellaneous";
    public static final String MISCELLANEOUS_ABSOLUTE_IMAGE_URL = "/images/miscellaneous/";
    public static final String MISCELLANEOUS_IMAGE_URL = "/images/miscellaneous/";
    public static final String PROVINCE_BASE_URL = "/admin/provinces";
    public static final String USER_BASE_URL = "/user";
    public static final String PROPERTY_TYPE_BASE_URL = "/admin/property-types";
    public static final String PHASE_BASE_URL = "/admin/phases";
    public static final String PROPERTY_BASE_URL = "/properties";
    public static final String PERSON_PROFILE_BASE_URL = "/admin/profile";
    public static final String PROFILE_IMAGE_ABSOLUTE_URL = "/images/profile/";
    public static final String PROFILE_IMAGE_URL = "/images/profile/";
    public static final String PROPERTY_IMAGE_ABSOLUTE_URL = "/images/property/";
    public static final String PROPERTY_IMAGE_URL = "/images/property/";
    public static final String PROPERTYSALERECORD_BASE_URL = "/properties/sale-records";
    public static final String REPORT_BASE_URL = "/admin/reports";
    public static final String SETTING_BASE_URL = "/admin/settings";
    public static final String SECTOR_BASE_URL = "/admin/sectors";
    public static final String SEARCH_BASE_URL = "/search";

    // Inventory URL's

   // public static final String INVENTORY = "/inventory";

    public static final String INVENTORY_STOCK = "/inventory-stock";

   // public static final String INVENTORY_ADJUSTMENT = INVENTORY + "/adjustment";

    public static final String ADD = "/add";
    public static final String VIEW ="/view";
    public static final String EDIT = "/edit/{id}";
    public static final String UPDATE = "/update/{id}";
    public static final String DELETE = "/delete/{id}";
    //public static final String REDIRECT_VIEW = "redirect:"+INVENTORY+VIEW;
    //public static final String REDIRECT_INVENTORY = "redirect:/"+INVENTORY;
    public static final String EDIT_INVENTORY = "inventory/edit-inventory";
    public static final String INVENTORY_LIST = "inventory/inventory-list";
    public static final String INVENTORY_FORM = "inventory/inventory-form";

    public static final String CASH_FLOW = "/cash-flow";

    public static final String EXPENSE = "/expense";

    public static final String REPORT = "/report";

    public static final String MONTHLY_REPORT = "/monthly-report";

    public static final String CHART = "/chart";

    public static final String REVENUE_COMPARISON_CHART = "/revenue-comparison-chart";

    public static final String EXPENSE_COMPARISON_CHART = "/expense-comparison-chart";

    public static final String EXPENSE_AND_PROFIT_CHART = "/expense-profit-chart";

    public static final String MONTHLY_REVENUE = "/monthly-revenue";

    public static final String ANNUAL_REVENUE = "/annual-revenue";

    public static final String ANNUAL_EXPENSE = "/annual-expense";

    public static final String MONTHLY_EXPENSE = "/monthly-expense";


    public static final String PURCHASE_EXPENSE = "/monthly-purchase-expense";

    public static final String ANNUAL_PURCHASE_EXPENSE = "/annual-purchase-expense";

    public static final String COMPARISON_CHART = "";




    ////////////////////////////////////////////////////////////////
    ////////// ****** FINANCE URL ROUTES ****** ///////////////////
    //////////////////////////////////////////////////////////////

    public static final String FINANCE  = "/Finance";
    public static final String ACCOUNTTYPE = FINANCE + "/AccountType";
    public static final String ACCOUNTGROUP = FINANCE+"/AccountGroup";
    public static final String ACCOUNTSUBGROUP = FINANCE+"/AccountSubGroup";
    public static final String ACCOUNTHEAD = FINANCE+"/AccountHead";


    public static final String ORGANIZATION = "/Organization";
    public static final String COMPANY = ORGANIZATION +"/Company";
    public static final String CUSTOMER = "/Customer";
    public static final String BRANCH = ORGANIZATION + "/Branch";
    public static final String DEPARTMENT = ORGANIZATION + "/Department";
    public static final String DESIGNATION = ORGANIZATION+"/Designation";


    public static final String HRM = "/HRM";
    public static final String EMPLOYEE = HRM+"/Employee";
    public static final String MEMBER = "/Member";
    public static final String COACH =  "/Coach";
    public static final String PACKAGE = "/Package";
    public static final String BRAND = "/Brand";
    public static final String CATEGORY = "/Category";
    public static final String PRODUCT = "/Product";
    public static final String INVENTORY = "/Inventory";

}