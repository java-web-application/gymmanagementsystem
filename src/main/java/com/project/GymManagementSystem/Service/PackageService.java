package com.project.GymManagementSystem.Service;

import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Repository.MemberRepository;
import com.project.GymManagementSystem.Repository.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PackageService {

    @Autowired
    PackageRepository packageRepository;

    public List<Package> findAll(){
        return packageRepository.findAll();
    }

    public Optional<Package> findById(Integer packageId){
        return packageRepository.findById(packageId);
    }

    public void save(Package pkg){
        packageRepository.save(pkg);
    }

    public void deleteById(Integer packageId){
        packageRepository.deleteById(packageId);
    }


}
