package com.project.GymManagementSystem.Service;


import com.project.GymManagementSystem.Model.Supplier;
import com.project.GymManagementSystem.Model.SupplierViewInterface;
import com.project.GymManagementSystem.Repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    public List<Supplier> findAll(){
        return supplierRepository.findAll();
    }

    public List<SupplierViewInterface> findAllAsSupplierViewInterface(){
        return supplierRepository.findAllBy();
    }

    public Supplier findById(int supplierId){
        return supplierRepository.findById(supplierId);
    }

    public void saveOrUpdate(Supplier supplier){
        supplierRepository.save(supplier);
    }
    public void deleteById(int supplierId){
        supplierRepository.deleteById(supplierId);
    }
}
