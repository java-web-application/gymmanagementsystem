package com.project.GymManagementSystem.Repository;

import com.project.GymManagementSystem.Model.Brand;
import com.project.GymManagementSystem.Model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

    public List<Category> findAll();

    public Optional<Category> findById(Integer categoryId);
}
