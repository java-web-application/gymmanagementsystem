package com.project.GymManagementSystem.Controller;

import com.project.GymManagementSystem.Model.Coach;
import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Service.CoachService;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.COACH)
public class CoachController extends BaseController{

    @Autowired
    CoachService coachService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.COACHLIST).addObject("Coaches", coachService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.COACH);
        modelAndView.addObject("Coach",new Member());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Coach") @Valid Coach coach, BindingResult bindingResult){
        coachService.save(coach);
        return redirectTo(UrlRoutes.COACH);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer coachId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Coach Id = "+coachId);
        Optional<Coach> coach = coachService.findById(coachId);
        ModelAndView modelAndView = new ModelAndView(View.COACH);
        modelAndView.addObject("Coach",coach);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer coachId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Coach Id = "+coachId);
        coachService.deleteById(coachId);
        return redirectTo(UrlRoutes.COACH);
    }

}