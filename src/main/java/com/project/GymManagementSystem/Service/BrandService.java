package com.project.GymManagementSystem.Service;

import com.project.GymManagementSystem.Model.Brand;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Repository.BrandRepository;
import com.project.GymManagementSystem.Repository.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandService {

    @Autowired
    BrandRepository brandRepository;

    public List<Brand> findAll(){
        return brandRepository.findAll();
    }

    public Optional<Brand> findById(Integer brandId){
        return brandRepository.findById(brandId);
    }

    public void save(Brand brand){
        brandRepository.save(brand);
    }

    public void deleteById(Integer brandId){
        brandRepository.deleteById(brandId);
    }


}
