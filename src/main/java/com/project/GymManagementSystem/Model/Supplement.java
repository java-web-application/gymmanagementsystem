package com.project.GymManagementSystem.Model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Supplement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String code;

    private String name;

    @OneToOne
    private Brand brand;

    private String cost;

    private String price;

    private Date MFGDate;

    private Date EXPDate;

    private String imageUrl;

    @OneToOne
    private Supplier supplier;

}
