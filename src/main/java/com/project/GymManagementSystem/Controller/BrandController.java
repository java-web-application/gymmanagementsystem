package com.project.GymManagementSystem.Controller;

import com.project.GymManagementSystem.Model.Brand;
import com.project.GymManagementSystem.Model.Member;
import com.project.GymManagementSystem.Model.Package;
import com.project.GymManagementSystem.Service.BrandService;
import com.project.GymManagementSystem.Service.MemberService;
import com.project.GymManagementSystem.Service.PackageService;
import com.project.GymManagementSystem.Utils.DefaultUrlRoutes;
import com.project.GymManagementSystem.Utils.UrlRoutes;
import com.project.GymManagementSystem.Utils.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping(value = UrlRoutes.BRAND)
public class BrandController extends BaseController{

    @Autowired
    BrandService brandService;

    @RequestMapping(value = DefaultUrlRoutes.INDEX)
    public ModelAndView index(){
        return new ModelAndView(View.BRANDLIST).addObject("Brands",brandService.findAll());
    }

    @RequestMapping(value = DefaultUrlRoutes.CREATE)
    public ModelAndView create(){
        ModelAndView modelAndView = new ModelAndView(View.BRAND);
        modelAndView.addObject("Brand",new Brand());
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.SAVE)
    public ModelAndView save(@ModelAttribute("Brand") @Valid Brand brand, BindingResult bindingResult){
        brandService.save(brand);
        return redirectTo(UrlRoutes.BRAND);
    }

    @RequestMapping(value = DefaultUrlRoutes.EDIT)
    public ModelAndView edit(HttpServletRequest httpServletRequest){
        Integer brandId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Brand Id = "+brandId);
        Optional<Brand> brand = brandService.findById(brandId);
        ModelAndView modelAndView = new ModelAndView(View.BRAND);
        modelAndView.addObject("Brand",brand);
        return modelAndView;
    }

    @RequestMapping(value = DefaultUrlRoutes.DELETE)
    public ModelAndView delete(HttpServletRequest httpServletRequest){
        Integer brandId = Integer.valueOf(httpServletRequest.getParameter("id"));
        System.out.println("Brand Id = "+brandId);
        brandService.deleteById(brandId);
        return redirectTo(UrlRoutes.BRAND);
    }

}
