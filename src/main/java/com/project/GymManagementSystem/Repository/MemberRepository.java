package com.project.GymManagementSystem.Repository;

import com.project.GymManagementSystem.Model.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member,Integer> {

    public List<Member> findAll();

    public Optional<Member> findById(Integer memberId);

    public Member findByCode(String memberCode);
}
