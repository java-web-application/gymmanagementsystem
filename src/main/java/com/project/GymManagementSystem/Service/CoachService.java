package com.project.GymManagementSystem.Service;

import com.project.GymManagementSystem.Model.Coach;
import com.project.GymManagementSystem.Repository.CoachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CoachService {

    @Autowired
    CoachRepository coachRepository;

    public List<Coach> findAll(){
       return coachRepository.findAll();
    }

    public Coach findByCode(String coachCode){
        return coachRepository.findByCode(coachCode);
    }

    public Optional<Coach> findById(Integer coachId){
        return coachRepository.findById(coachId);
    }
    public void save(Coach coach){
        coachRepository.save(coach);
    }
    public void deleteById(Integer coachId){
        coachRepository.deleteById(coachId);
    }

}
