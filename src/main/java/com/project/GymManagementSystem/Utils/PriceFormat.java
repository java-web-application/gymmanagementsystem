package com.project.GymManagementSystem.Utils;

import java.math.BigDecimal;

public class PriceFormat {

    public static String getStringWithoutFractionFrom(BigDecimal value) {

        if (value != null) {
            String toBeConvertedValue = value.toString();
            if (toBeConvertedValue.indexOf('.') > -1) {
                String stringWithoutDecimals = toBeConvertedValue.substring(0, toBeConvertedValue.indexOf("."));
                return stringWithoutDecimals;
            } else {
                return Integer.toString(0);
            }

        }
        return "";
    }
}
