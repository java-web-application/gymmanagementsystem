package com.project.GymManagementSystem.Repository;


import com.project.GymManagementSystem.Model.Supplier;
import com.project.GymManagementSystem.Model.SupplierViewInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier,Integer> {

    public List<Supplier> findAll();

    public List<SupplierViewInterface> findAllBy();

    public Supplier findById(int supplierId);

}
